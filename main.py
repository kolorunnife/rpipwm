import time
import RPi.GPIO as GPIO

led = 7
trig = 40
echo = 37
MAX_DISTANCE = 0.7

#Setup GPIO Pins
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(led, GPIO.OUT)
GPIO.setup(trig, GPIO.OUT)
GPIO.setup(echo, GPIO.IN)

#Setup PWM pins
ledPWM = GPIO.PWM(7, 100)
ledPWM.start(0)

def intensity(volume):
    ledPWM.ChangeDutyCycle(volume)

def getDistance():
    GPIO.output(trig, True);
    time.sleep(0.0001);
    GPIO.output(trig, False)
            
    while GPIO.input(echo) == False:
        start = time.time()

    while GPIO.input(echo) == True:
        end = time.time()

    timeInterval = end - start

    return (timeInterval / 0.000058) / 100

#PROGRAM

while True:
    distance = getDistance()

    if (distance >= MAX_DISTANCE):
        distance = MAX_DISTANCE
    
    dutyCycle = 99 * (distance / MAX_DISTANCE)
    print("Distance: " + str(distance) + "/" + str(MAX_DISTANCE) +  "m & Brightness Percentage: " + str(dutyCycle))
    intensity(dutyCycle)
    time.sleep(0.1)

GPIO.cleanup()